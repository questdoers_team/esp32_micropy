# coding: utf-8

"""Blink LED on ESP."""

import utime
from machine import Pin

blink_pin = Pin(2, Pin.OUT)

while True:
    blink_pin.value(0)
    utime.sleep_ms(1000)
    blink_pin.value(1)
    utime.sleep_ms(100)
    blink_pin.value(0)
    utime.sleep_ms(100)
    blink_pin.value(1)
    utime.sleep_ms(500)
    blink_pin.value(0)
    utime.sleep_ms(500)
    blink_pin.value(1)
    utime.sleep_ms(500)
