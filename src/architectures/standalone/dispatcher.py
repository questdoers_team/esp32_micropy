# coding: utf-8


class Dispatcher:
    def __init__(self, message_queue):
        self.message_queue = message_queue

    def dispatch_event(self, package):
        self.publish(package)

    def dispatch_message(self, topic, msg, retain, qos):
        print('Message published')

    def publish(self, package):
        self.message_queue.message_queue.append(package)
