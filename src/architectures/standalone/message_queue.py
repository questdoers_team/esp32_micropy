# coding: utf-8

from dispatcher import Dispatcher


class InternalMessageQueue:
    def __init__(self):
        self.dispatcher = Dispatcher(self)
        self.callback = None
        self.message_queue = []

    def set_callback(self, callback):
        self.callback = callback

    def publish(self, topic, msg, retain=False, qos=0):
        self.dispatcher.dispatch_message(topic, msg, retain, qos)

    def check_msg(self):
        if len(self.message_queue):
            message = self.message_queue.pop(0)
            if self.callback:
                self.callback(message)

    def publish_event(self, call, args=None):
        package = {}
        package['call'] = call
        if args:
            package['args'] = args
        self.dispatcher.dispatch_event(package)
