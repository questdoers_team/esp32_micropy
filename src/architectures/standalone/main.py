# coding: utf-8

from app import App
from message_queue import InternalMessageQueue

client = InternalMessageQueue()

client.set_callback(
    lambda package: application.on_message(package),
)

application = App(client)

application.start()
while True:
    client.check_msg()
    application.loop()
