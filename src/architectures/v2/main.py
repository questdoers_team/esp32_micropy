# coding: utf-8

import machine
import network
import ujson
import utime
from app import App
from mqtt import MQTTClient

nic = network.WLAN()
config_file = open('config.json')
config = ujson.loads(config_file.read())
config_file.close()

while not nic.isconnected():
    print('connecting')
    utime.sleep(1)

client = MQTTClient(
    topic=config['topic'],
    server=config['mqtt_host'],
    port=1883,
    user=config['mqtt_user'],
    password=config['mqtt_password'],
    client_id=nic.config('mac'),
    keepalive=60,
)

application = App(client)

client.set_callback(
    lambda package: application.on_message(package),
)

client.mqtt.set_last_will(
    topic='lwt',
    msg='{"from":"{0}","class":"event","call":"bye"}'.format(config['topic']),
    qos=1,
)

try:
    client.mqtt.connect()
except OSError as error:
    print(error)
    print('connecting')
    utime.sleep(60)
    machine.reset()

client.subscribe(config['topic'].encode('utf-8'), qos=1)
start_tick = utime.ticks_ms()
print(start_tick)

application.start()
while True:
    try:
        if utime.ticks_ms() - start_tick > 30000:
            start_tick = utime.ticks_ms()
            client.mqtt.ping()
        client.mqtt.check_msg()
    except Exception as error:
        print(error)
    application.loop()
