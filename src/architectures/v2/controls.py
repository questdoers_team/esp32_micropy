# coding: utf-8

import utime
from machine import ADC, Pin


class Button:
    """Button encapsulates button with it's state."""

    def __init__(self, pin):
        """
        Constructor for button.

        param pin: The digit of button pin.
        """
        self.pin = pin
        self.is_pressed = False

    def check_click(self, delay=10):
        """
        Check if button was clicked.
        """
        if self.pin.value() == 0:
            utime.sleep_ms(delay)
            if self.pin.value() == 0:
                if not self.is_pressed:
                    self.is_pressed = True
                    return True
                return False
        self.is_pressed = False
        return False


class AnalogDigitalPin:
    def __init__(self, pin, pull=None, treshold=512):
        self.pin = ADC(Pin(pin))
        self.actual_pull = pull
        self.pull = None
        self.treshold = treshold

    def init(
        self,
        mode=-1,
        pull=-1,
        value=-1,
        drive=-1,
        alt=-1,
    ):
        if mode != -1 and mode != Pin.IN:
            raise ValueError('Mode of AnalogDigitalPin can be only Pin.IN')
        if pull != -1:
            self.pull = pull
        if value != -1:
            self.value(value)

    def __call__(self, value=None):
        return self.value(value)

    def value(self, value=None):
        if value:
            raise ValueError('AnalogDigitalPin can\'t be setted')
        if self.actual_pull is None and self.pull is not None:
            raise ValueError('AnalogDigitalPin misconfigured')
        if self.pull is None and self.actual_pull is not None:
            raise ValueError('AnalogDigitalPin misconfigured')
        if self.actual_pull == self.pull:
            return int(self.treshold <= self.pin.read())
        return int(self.treshold >= self.pin.read())

    def on(self):
        raise ValueError('AnalogDigitalPin can\'t be setted')

    def off(self):
        raise ValueError('AnalogDigitalPin can\'t be setted')

    def mode(self, mode=None):
        if not mode:
            return Pin.IN
        if mode != Pin.OUT:
            raise ValueError('Mode of AnalogDigitalPin can be only Pin.IN')

    def pull(self, pull=None):
        if not pull:
            return None
        self.pull = pull


class Relay:
    def __init__(self, pin, class_='relay', value=0):
        self.pin = pin
        self.pin.init(Pin.OUT, value=value)
        self.class_ = class_

    def on_message(self, package):
        if self.class_ == package['class']:
            if package['call'] == 'on':
                self.pin.value(1)
            if package['call'] == 'off':
                self.pin.value(0)


class ButtonUp:
    """Button that reacts on up encapsulates button with it's state."""

    def __init__(self, pin, read_timeout=100):
        """
        Constructor for button.

        param pin: The digit of button pin.
        """
        self.pin = pin
        self.is_pressed = False
        self.read_timeout = read_timeout
        self.start_time = utime.ticks_ms()

    def check_click(self):
        """
        Check if button was clicked.
        """
        is_clicked = False
        if utime.ticks_ms() - self.start_time > self.read_timeout:
            if not self.is_pressed and self.pin.value() == 0:
                self.is_pressed = True
                is_clicked = False
            if self.is_pressed and self.pin.value() == 1:
                self.is_pressed = False
                is_clicked = True
            self.start_time = utime.ticks_ms()
        return is_clicked
