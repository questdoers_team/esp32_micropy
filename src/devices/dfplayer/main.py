# coding: utf-8

"""Test DFPlayer."""

import machine
import utime
from dfplayer import DFPlayer

df = DFPlayer(machine.UART(1, 9600, tx=10, rx=9), 13)

df.set_volume(14)
df.play_track()
while True:
    utime.sleep(3)
    df.next_track()
