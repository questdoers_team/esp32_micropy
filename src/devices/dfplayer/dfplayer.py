# coding: utf-8

"""
Module for DFplayer Mini Player.

Datasheet: http://www.picaxe.com/docs/spe033.pdf
"""

import utime
from machine import Pin


def _count_control_sum(command: bytes):
    """
    Count control sum.

    The control sum of command is a sum of
    all command bytes substracted from 2^16.

    control_sum = 2^16 - (b0 + b1 + ... b6).
    """
    control_sum = 0
    for byte in command:
        control_sum += byte
    control_sum = 2**16 - control_sum
    up = control_sum >> 8
    down = control_sum & 255
    return bytes([up, down])


class DFPlayer:
    """Class for DFplayer Mini Player."""

    def __init__(self, uart, busy: int):
        """
        Init DFPlayer.

        :param uart: machine.UART of player
        :param busy: int pin
        """
        self.uart = uart
        self.busy = Pin(busy, Pin.IN)
        self.uart.init(9600, bits=8, parity=None, stop=1)
        self.reset()

    def play_track(self, track: int):
        """
        Play track.

        :param track: track in [0, 255].
            Tracks should be in "mp3" directory and
            have names "001.mp3", "002.mp3", etc.
            Track is the order number of track.
            The order number depends of order of
            writing file to sdcard.
            It is recomended to write files separately
            from the first to the last one.
        """
        if not (0 <= track <= 255):
            raise ValueError('Track must be 0-255')
        command = b'\xFF\x06\x03\x00\x00' + bytes([track])
        self.send(command)
        utime.sleep_ms(50)

    def pause(self):
        """Pause player."""
        self.send(b'\xFF\x06\x0E\x00\x00\x00')
        utime.sleep_ms(50)

    def is_busy(self):
        """Return True if track is playing."""
        return self.busy.value() == 0

    def reset(self):
        """Reset player."""
        self.send(b'\xFF\x06\x0C\x00\x00\x00')
        utime.sleep_ms(50)

    def next_track(self):
        """Play next track."""
        self.send(b'\xFF\x06\x01\x00\x00\x00')
        utime.sleep_ms(50)

    def set_volume(self, volume: int):
        """
        Set volume to player.

        :param volume: volume in [0, 30]
        """
        if not (0 <= volume <= 30):
            raise ValueError('Volume must be 0-30')
        command = b'\xFF\x06\x06\x00\x00' + bytes([volume])
        self.send(command)
        utime.sleep_ms(50)

    def send(self, command: bytes):
        """Send command to DFPlayer by uart."""
        control_sum = _count_control_sum(command)
        message = b'\x7E' + command + control_sum + b'\xEF'
        for byte in message:
            self.uart.write(bytes([byte]))


class MP3:
    """MP3 player that can be managed remotely and localy."""

    def __init__(
        self,
        player: DFPlayer,
        client: object,
        class_: str = 'mp3',
    ):
        """
        Init MP3.

        player: hardware player control class.
        client: mqtt client.
        class_: package key "class" that conlrols player.
        """
        self.player = player
        self.class_ = class_
        self.client = client

        self.was_player_started = False

        self.volume = 0

        utime.sleep_ms(100)
        self.set_volume(30)

    def on_message(self, package):
        """Message handler."""
        if self.class_ == package['class']:
            if package['call'] == 'play':
                args = package['args']
                self.was_player_started = False
                self.play(
                    track=int(args['track']),
                    volume=int(args['volume']),
                )
                self.was_player_started = True
            if package['call'] == 'pause':
                self.player.pause()
                self.was_player_started = False

    def play(self, track, volume=None):
        """
        Play track without blocking thread.

        :param track: track in [0, 255].
        :param volume: volume in [0, 30] or None for use previous value.
        """
        if volume:
            self.set_volume(volume)
        self.player.play_track(track)
        if self.was_player_started:
            self.send_done()

    def play_block(self, track, volume=None):
        """
        Play track with blocking thread.

        :param track: track in [0, 255].
        :param volume: volume in [0, 30] or None for use previous value.
        """
        if volume:
            self.set_volume(volume)
        self.player.play_track(track)
        while self.player.is_busy():
            utime.sleep_ms(50)
        if self.was_player_started:
            self.send_done()

    def set_volume(self, volume):
        """
        Set volume to player.

        :param volume: volume in [0, 30]
        """
        self.volume = volume
        self.player.set_volume(self.volume)

    def loop(self):
        """Loop."""
        if not self.player.is_busy():
            if self.was_player_started:
                self.send_done()

    def send_done(self):
        """Send done package."""
        self.was_player_started = False
        self.client.publish_event(
            call=self.class_ + '_finished',
        )
