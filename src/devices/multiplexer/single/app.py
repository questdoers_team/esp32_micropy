# coding: utf-8

import gc
import time
from multiplexer import Multiplexer


class App:
    def __init__(self, client, topic):
        self.client = client
        self.topic = topic
        self.multiplexer = Multiplexer(36, [26, 25, 33, 32])

    def on_message(self, topic, message):
        print(topic)
        print(message)
        self.client.publish('micropython', 'hello')

    def loop(self):
        time.sleep_ms(1000)
        self.multiplexer.read_states()
        print(self.multiplexer.get_states())
        print(gc.mem_free())

    def start(self):
        pass
