# coding: utf-8

"""Module for interaction with shift register."""

from machine import Pin


class ShiftRegister:
    """
    Class for shift register.

    :param data_pin: shift register data pin.
    :param clock_pin: shift register clock pin.
    :param latch_pin: shift register latch pin.
    :param num_outs: number of outs.
    """

    def __init__(self, data_pin, clock_pin, latch_pin, num_outs):
        """
        Initialize shift register.
        """
        self.data_pin = Pin(data_pin, Pin.OUT, value=0)
        self.clock_pin = Pin(clock_pin, Pin.OUT, value=0)
        self.latch_pin = Pin(latch_pin, Pin.OUT, value=0)
        self.num_outs = num_outs
        self.outs_status = 0
        self.show_bits()

    def latch(self):
        """Switch latch."""
        self.latch_pin.value(1)
        self.latch_pin.value(0)

    def clock(self):
        """Switch clock."""
        self.clock_pin.value(1)
        self.clock_pin.value(0)

    def show_bits(self):
        """Write view bits to register."""
        outs_status = self.outs_status
        print(bin(outs_status))
        for _ in range(self.num_outs):
            self.data_pin.value(outs_status & 1)
            outs_status >>= 1
            self.clock()
        self.latch()

    def assign_bit(self, position, bit_value, auto_show=True):
        """
        Assign bit to shift register view.

        :param position: out position (from most significat bit, MSB=0).
        :param bit_value: value for write out.
        :param auto_show: write imidiately flag.
        """
        if bit_value:
            self.outs_status |= (1 << (self.num_outs - position - 1))
        else:
            self.outs_status &= ~(1 << (self.num_outs - position - 1))
        if auto_show:
            self.show_bits()

    def get_position(self, position):
        """Get value of position."""
        return (self.outs_status & (1 << position)) >> position


class ShiftRegisterPin:
    """
    ShiftRegisterPin allow to manipulate separate shift register pin.

    :param shift_register: ShiftRegister instance.
    :param position: position of pin in register
                     (from most significat bit, MSB=0).
    """

    OUT = Pin.OUT

    def __init__(self, shift_register, position):
        """Initialize shift register pin."""
        self.shift_register = shift_register
        self.position = position

    def init(
        self,
        mode=-1,
        pull=-1,
        value=-1,
        drive=-1,
        alt=-1,
    ):
        """
        Pin.init method fake.

        It can set mode to Pin.OUT.
        It can set value if present.

        See machine.Pin constructor documentation for details
        of the mode argument.
        """
        if mode != -1 and mode != Pin.OUT:
            raise ValueError('Mode of ShiftRegisterPin can be only Pin.OUT')
        if pull != -1:
            raise ValueError('ShiftRegisterPin can\'t be pulled')
        if value != -1:
            self.value(value)

    def __call__(self, value=None):
        """
        Set or get the value of the pin.

        :param value: the value that will be assign to bit 
                      that concern to pin.
        """
        return self.value(value)

    def value(self, value=None):
        """
        Set or get the value of the pin.

        :param value: the value that will be assign to bit
                      that concern to pin.
        """
        if value is not None:
            self.shift_register.assign_bit(self.position, value)
            return value
        return self.shift_register.get_position(self.position)

    def on(self):
        """Set pin to "1" output level."""
        self.value(1)

    def off(self):
        """Set pin to "0" output level."""
        self.value(0)

    def mode(self, mode=None):
        """Get or set the pin mode."""
        if not mode:
            return Pin.OUT
        if mode != Pin.OUT:
            raise ValueError('Mode of ShiftRegisterPin can be only Pin.OUT')

    def pull(self, pull=None):
        """Get or set the pin pull state."""
        if not pull:
            return None
        raise ValueError('ShiftRegisterPin can\'t be pulled')
