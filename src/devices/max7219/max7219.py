# coding: utf-8

# MIT License
# Copyright (c) 2017 Mike Causer
# Copyright (c) 2019 Escape Room Makers
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


"""
MicroPython max7219 cascadable 8x8 LED matrix driver.

Based on https://github.com/mcauser/micropython-max7219
"""

import framebuf
from micropython import const

_NOOP = const(0)
_DIGIT0 = const(1)
_DECODEMODE = const(9)
_INTENSITY = const(10)
_SCANLIMIT = const(11)
_SHUTDOWN = const(12)
_DISPLAYTEST = const(15)


class Matrix8x8:
    """
    Driver for cascading MAX7219 8x8 LED matrices.

    >>> import max7219
    >>> from machine import Pin, SPI
    >>> spi = SPI(1)
    >>> display = max7219.Matrix8x8(spi, Pin('X5'), 4)
    >>> display.text('1234',0,0,1)
    >>> display.show()
    """

    def __init__(self, spi, cs, num):
        """Init max7219 matrix 8x8."""
        self.spi = spi
        self.cs = cs
        self.cs.init(cs.OUT, 1)
        self.buffer = bytearray(8 * num)
        self.num = num
        fb = framebuf.FrameBuffer(self.buffer, 8 * num, 8, framebuf.MONO_HLSB)
        self.framebuf = fb
        # Provide methods for accessing FrameBuffer graphics primitives.
        # This is a workround because inheritance from a native class is
        # currently unsupported.
        # http://docs.micropython.org/en/latest/pyboard/library/framebuf.html
        self.fill = fb.fill  # (col)
        self.pixel = fb.pixel  # (x, y[, c])
        self.hline = fb.hline  # (x, y, w, col)
        self.vline = fb.vline  # (x, y, h, col)
        self.line = fb.line  # (x1, y1, x2, y2, col)
        self.rect = fb.rect  # (x, y, w, h, col)
        self.fill_rect = fb.fill_rect  # (x, y, w, h, col)
        self.text = fb.text  # (string, x, y, col=1)
        self.scroll = fb.scroll  # (dx, dy)
        self.blit = fb.blit  # (fbuf, x, y[, key])
        self.init()

    def _write(self, command, argument):
        self.cs(0)
        for _ in range(self.num):
            self.spi.write(
                bytearray([command, argument]),
            )
        self.cs(1)

    def draw_sign(self, sign):
        """Draw sign in format [x, y, x, y...]."""
        for index in range(len(sign) / 2):
            self.pixel(
                sign[2 * index],
                sign[2 * index + 1],
                1,
            )

    def init(self):
        """Initialize max7219."""
        for command, argument in (
            (_SHUTDOWN, 0),
            (_DISPLAYTEST, 0),
            (_SCANLIMIT, 7),
            (_DECODEMODE, 0),
            (_SHUTDOWN, 1),
        ):
            self._write(command, argument)

    def brightness(self, brightness):
        """Brightness was deprecated use set_brightness instead."""
        raise NotImplementedError(
            'brightness was deprecated use set_brightness instead.',
        )

    def set_brightness(self, brightness):
        """Set matrix brightness."""
        if not 0 <= brightness <= 15:
            raise ValueError('Brightness out of range')
        self._write(_INTENSITY, brightness)

    def show(self):
        """Show buffer on led matrix."""
        for row in range(8):
            self.cs(0)
            for col in range(self.num):
                self.spi.write(
                    bytearray(
                        [_DIGIT0 + row, self.buffer[(row * self.num) + col]],
                    ),
                )
            self.cs(1)
