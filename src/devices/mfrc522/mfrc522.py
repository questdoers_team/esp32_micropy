# coding: utf-8

# The MIT License (MIT).
#
# Copyright (c) 2016 Stefan Wendler
# Copyright (c) 2019 Escape Room Makers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Module for interaction with MFRC522 RFID reader."""

from machine import Pin

OK = 0
NOTAGERR = 1
ERR = 2

REQIDL = 0x26
REQALL = 0x52
AUTHENT1A = 0x60
AUTHENT1B = 0x61


def get_command_irq_en(cmd):
    """Get command enable IRQ."""
    irq_en = 0
    if cmd == 0xE:
        irq_en = 0x12
    elif cmd == 0xC:
        irq_en = 0x77
    return irq_en


def get_command_wait_irq(cmd):
    """Get command wait IRQ."""
    wait_irq = 0
    if cmd == 0xE:
        wait_irq = 0x10
    elif cmd == 0xC:
        wait_irq = 0x30
    return wait_irq


class MFRC522:
    """Class for interction with MFRC522 RFID reader."""

    def __init__(self, spi, rst, cs):
        """Initialize MFRC522 spi."""
        self.rst = Pin(rst, Pin.OUT)
        self.cs = Pin(cs, Pin.OUT)

        self.rst.value(0)
        self.cs.value(1)

        self.spi = spi
        self.rst.value(1)
        self.init()

    def _write_register(self, register, register_value):
        self.cs.value(0)

        self.spi.write(
            b'%c' % int(
                0xff & ((register << 1) & 0x7e),
            ),
        )
        self.spi.write(b'%c' % int(0xff & register_value))
        self.cs.value(1)

    def _read_register(self, register):

        self.cs.value(0)
        self.spi.write(
            b'%c' % int(
                0xff & (
                    ((register << 1) & 0x7e) | 0x80
                ),
            ),
        )
        register_value = self.spi.read(1)
        self.cs.value(1)

        return register_value[0]

    def _set_flags(self, reg, mask):
        self._write_register(
            reg,
            self._read_register(reg) | mask,
        )

    def _clear_flags(self, reg, mask):
        self._write_register(
            reg,
            self._read_register(reg) & (~mask),
        )

    def _tocard(self, cmd, send):

        recv = []
        bits = 0
        irq_en = get_command_irq_en(cmd)
        wait_irq = get_command_wait_irq(cmd)
        register_data = 0
        stat = ERR

        self._write_register(0x02, irq_en | 0x80)
        self._clear_flags(0x04, 0x80)
        self._set_flags(0x0A, 0x80)
        self._write_register(0x01, 0x00)

        for char in send:
            self._write_register(0x09, char)
        self._write_register(0x01, cmd)

        if cmd == 0x0C:
            self._set_flags(0x0D, 0x80)

        iteration = 2000
        while True:
            register_data = self._read_register(0x04)
            iteration -= 1
            condition = (iteration != 0) and (
                ~(register_data & 0x01) and ~(register_data & wait_irq),
            )
            if ~(condition):
                break

        self._clear_flags(0x0D, 0x80)

        if iteration:
            stat, recv, bits = self._do_tocard(cmd, register_data, irq_en)
        return stat, recv, bits

    def _do_tocard(self, cmd, register_data, irq_en):
        stat = ERR
        recv = []
        bits = 0
        if (self._read_register(0x06) & 0x1B) == 0x00:
            stat = OK
            if register_data & irq_en & 0x01:
                stat = NOTAGERR
            elif cmd == 0x0C:
                register_data = self._read_register(0x0A)
                lbits = self._read_register(0x0C) & 0x07
                if lbits == 0:
                    bits = register_data * 8
                else:
                    bits = (register_data - 1) * 8 + lbits

                if register_data == 0:
                    register_data = 1
                if register_data > 16:
                    register_data = 16

                for _ in range(register_data):
                    recv.append(self._read_register(0x09))
        else:
            stat = ERR
        return stat, recv, bits

    def _crc(self, data_for_crc):
        """Count crc."""
        self._clear_flags(0x05, 0x04)
        self._set_flags(0x0A, 0x80)

        for char in data_for_crc:
            self._write_register(0x09, char)

        self._write_register(0x01, 0x03)

        iteration = 0xFF
        while True:
            readed_data = self._read_register(0x05)
            iteration -= 1
            if not ((iteration != 0) and not (readed_data & 0x04)):
                break

        return [self._read_register(0x22), self._read_register(0x21)]

    def init(self):
        """Init reader."""
        self.reset()
        self._write_register(0x2A, 0x8D)
        self._write_register(0x2B, 0x3E)
        self._write_register(0x2D, 30)
        self._write_register(0x2C, 0)
        self._write_register(0x15, 0x40)
        self._write_register(0x11, 0x3D)
        self.enable_antenna()

    def reset(self):
        """Reset reader."""
        self._write_register(0x01, 0x0F)

    def enable_antenna(self, on=True):
        """Enable antena."""
        if on and ~(self._read_register(0x14) & 0x03):
            self._set_flags(0x14, 0x03)
        else:
            self._clear_flags(0x14, 0x03)

    def request(self, mode):
        """Send request."""
        self._write_register(0x0D, 0x07)
        (stat, recv, bits) = self._tocard(0x0C, [mode])

        if (stat != self.OK) | (bits != 0x10):
            stat = ERR

        return stat, bits

    def anticoll(self):
        """Anticoll."""
        ser_chk = 0

        self._write_register(0x0D, 0x00)
        (stat, recv, bits) = self._tocard(
            0x0C,
            [0x93, 0x20],
        )

        if stat == OK:
            if len(recv) == 5:
                for index in range(4):
                    ser_chk = ser_chk ^ recv[index]
                if ser_chk != recv[4]:
                    stat = ERR
            else:
                stat = ERR

        return stat, recv

    def select_tag(self, ser):
        """Select tag."""
        buf = [0x93, 0x70] + ser[:5]
        buf += self._crc(buf)
        (stat, recv, bits) = self._tocard(0x0C, buf)
        return OK if (stat == OK) and (bits == 0x18) else ERR

    def auth(self, mode, addr, sect, ser):
        """Auth card."""
        send_data = [mode, addr] + sect + ser[:4]
        return self._tocard(0x0E, send_data)[0]

    def stop_crypto1(self):
        """Stop crypto1."""
        self._clear_flags(0x08, 0x08)

    def read(self, addr):
        """Read data from register."""
        data_for_send = [0x30, addr]
        data_for_send += self._crc(data_for_send)
        (stat, recv, _) = self._tocard(0x0C, data_for_send)
        return recv if stat == OK else None

    def write(self, addr, write_data):
        """Write data to register."""
        buf = [0xA0, addr]
        buf += self._crc(buf)
        (stat, recv, bits) = self._tocard(0x0C, buf)

        if check_write_error(stat, recv, bits):
            stat = ERR
        else:
            buf = []
            for index in range(16):
                buf.append(write_data[index])
            buf += self._crc(buf)
            (stat, recv, bits) = self._tocard(0x0C, buf)
            if check_write_error(stat, recv, bits):
                stat = ERR

        return stat


def check_write_error(stat, recv, bits):
    """Check write error."""
    if not (stat == OK):
        return True
    if not (bits == 4):
        return True
    if not ((recv[0] & 0x0F) == 0x0A):
        return True
    return False


class RFID:
    """
    Create RFID.

    :param spi: Preconfigured instance of machine.SPI.
        For esp32
        SPI(2, baudrate=10000000, polarity=0, phase=0,
        sck=Pin(18), mosi=Pin(23), miso=Pin(19))
    :param rst: Reset RFID pin.
    :param cs: CS (SS, SDA) RFID pin.

    """

    def __init__(self, spi, rst, cs):
        """Class for interaction with MFRC522 RFID reader."""
        self.mfrc = MFRC522(spi, rst, cs)

    def read(self):
        """Read card uid."""
        read_data = []
        (stat, tag_type) = self.mfrc.request(self.mfrc.REQIDL)
        if stat == self.mfrc.OK:
            (stat, raw_uid) = self.mfrc.anticoll()
            if stat == self.mfrc.OK:
                print('New card detected')
                print('  - tag type: ', hex(tag_type))
                print(
                    '  - uid	 : ',
                    (
                        raw_uid[0],
                        raw_uid[1],
                        raw_uid[2],
                        raw_uid[3],
                    ),
                )
                read_data = raw_uid[:4]
                print('')
        return read_data
