# coding: utf-8

"""Buzzer test."""

import buzzer

tempo = 5000
tones = {
    'c': buzzer.C4,
    'd': buzzer.D4,
    'e': buzzer.E4,
    'f': buzzer.F4,
    'g': buzzer.G4,
    'a': buzzer.A4,
    'b': buzzer.B4,
    'C': buzzer.C5,
    ' ': 0,
}
beeper = buzzer.Buzzer(21)
melody = [tones[tone] for tone in 'cdefgabC']
rhythm = [8, 8, 8, 8, 8, 8, 8, 8]

beeper.play_melody(melody, rhythm, tempo)
