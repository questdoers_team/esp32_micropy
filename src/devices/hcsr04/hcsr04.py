# coding: utf-8

# Copyright 2017 Roberto Sánchez
# Modifications copyright 2019 Escaperoommakers

"""
Module to use the untrasonic sensor HC-SR04.

Datasheet:
https://www.mpja.com/download/hc-sr04_ultrasonic_module_user_guidejohn.pdf
"""

import machine
import utime
from machine import Pin

# ECHO_TIMEOUT_US is based in chip range limit (400cm)
DEFAULT_ECHO_TIMEOUT_US = 30000
ETIMEDOUT = 110
# To calculate the distance we get the pulse_time and divide it by 2
# (the pulse walk the distance twice) and by 29.1 becasue
# the sound speed on air (343.2 m/s), that It's equivalent to
# 0.34320 mm/us that is 1mm each 2.91us
# pulse_time // 2 // 2.91 ->
# pulse_time // 5.82 ->
# pulse_time * 100 // 582
MM_DISTANCE_DIVIDER = 582
# To calculate the distance we get the pulse_time and divide it by 2
# (the pulse walk the distance twice) and by 29.1 becasue
# the sound speed on air (343.2 m/s), that It's equivalent to
# 0.034320 cm/us that is 1cm each 29.1us
CM_DISTANCE_DIVIDER = 29.1


class HCSR04:
    """
    Driver to use the untrasonic sensor HC-SR04.

    The sensor range is between 2cm and 4m.
    The timeouts received listening to echo pin are
    converted to OSError('Out of range').

    By default is based in sensor limit range (4m).

    :param trigger_pin: Output pin to send pulses
    :param echo_pin: Readonly pin to measure the distance.
                    The pin should be protected with 1k resistor.
    :param echo_timeout_us: Timeout in microseconds to listen to echo pin.
    """

    def __init__(
        self,
        trigger_pin,
        echo_pin,
        echo_timeout_us=DEFAULT_ECHO_TIMEOUT_US,
    ):
        """Initialize HCSR04."""
        self.echo_timeout_us = echo_timeout_us
        # Init trigger pin (out)
        self.trigger = Pin(trigger_pin, mode=Pin.OUT, pull=None)
        self.trigger.value(0)

        # Init echo pin (in)
        self.echo = Pin(echo_pin, mode=Pin.IN, pull=None)

    def _send_pulse_and_wait(self):
        """
        Send the pulse to trigger and listen on echo pin.

        We use the method `machine.time_pulse_us()`
        to get the microseconds until the echo is received.
        """
        self.trigger.value(0)  # Stabilize the sensor
        utime.sleep_us(5)
        self.trigger.value(1)
        # Send a 10us pulse.
        utime.sleep_us(10)
        self.trigger.value(0)
        try:
            pulse_time = machine.time_pulse_us(
                self.echo,
                1,
                self.echo_timeout_us,
            )
            return pulse_time
        except OSError as ex:
            if ETIMEDOUT == ex.args[0]:
                raise OSError('Out of range')
            raise ex

    def distance_mm(self):
        """
        Get the distance in milimeters without floating point operations.

        :return: distance in mm (int)
        """
        pulse_time = self._send_pulse_and_wait()
        mm = pulse_time * 100 // MM_DISTANCE_DIVIDER
        return mm

    def distance_cm(self):
        """
        Get the distance in centimeters with floating point operations.

        :return: distance in cm (float)
        """
        pulse_time = self._send_pulse_and_wait()

        cms = (pulse_time / 2) / CM_DISTANCE_DIVIDER
        return cms
