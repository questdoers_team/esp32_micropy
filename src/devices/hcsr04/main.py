# coding: utf-8

import utime
from hcsr04 import HCSR04

sensor = HCSR04(trigger_pin=19, echo_pin=5)

while True:
    distance = sensor.distance_cm()
    print('Distance:', distance, 'cm')
    utime.sleep(1)