# coding: utf-8

import max7219
from machine import Pin, SPI, UART

import gc
import time


class App:
    def __init__(self, client):
        self.client = client
        self.spi = SPI(
            2,
            baudrate=10000000,
            polarity=0,
            phase=0,
            sck=Pin(18),
            mosi=Pin(23),
            miso=Pin(19),
        )
        self.display = max7219.Matrix8x8(self.spi, Pin(5), 6)
        self.display.brightness(15)
        self.filled = 1
        self.display.fill(self.filled)
        self.display.show()

    def on_message(self, topic, message):
        print(topic)
        print(message)
        self.client.publish('micropython', 'hello')

    def loop(self):
        time.sleep_ms(1000)
        print(gc.mem_free())
        self.filled = 0 if self.filled else 1
        self.display.fill(self.filled)
        self.display.show()
