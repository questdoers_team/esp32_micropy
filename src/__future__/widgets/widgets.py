# coding: utf-8

"""Widgets for screens."""


class Text:
    def __init__(self):
        self.signs = []

    def collect_points(self):
        points = []
        offset = 0
        for sign in self.signs:
            points.extend(Sign.offset_x(sign[0], offset))
            offset += sign[1] + 1
        return points


class BlinkingText:
    def __init__(self):
        self.codes = []
        self.signs = []
        self.cursor = 0

    def update_signs(self):
        self.signs = []
        for code in self.codes:
            self.signs.append(Sign.dictionary[code])

    def collect_cursor_points(self):
        points = []
        offset = 0
        i = 0
        for sign in self.signs:
            if i != self.cursor:
                points.extend(Sign.offset_x(sign[0], offset))
            else:
                esign = []
                for i in range(8):
                    for j in range(sign[1]):
                        esign.append(j)
                        esign.append(i)
                points.extend(Sign.offset_x(esign, offset))
            offset += sign[1] + 1
            i += 1
        return points

    def collect_points(self):
        points = []
        offset = 0
        for sign in self.signs:
            points.extend(Sign.offset_x(sign[0], offset))
            offset += sign[1] + 1
        return points


class CenteredBlinkingText:
    def __init__(self, screen_len):
        self.codes = []
        self.signs = []
        self.cursor = 0
        self.screen_len = screen_len

    def update_signs(self):
        self.signs = []
        for code in self.codes:
            self.signs.append(Sign.dictionary[code])

    def collect_cursor_points(self):
        points = []
        line_len = 0
        for sign in self.signs:
            line_len += sign[1] + 1
        offset = (self.screen_len - line_len) // 2
        i = 0
        for sign in self.signs:
            if i != self.cursor:
                points.extend(Sign.offset_x(sign[0], offset))
            else:
                esign = []
                for i in range(8):
                    for j in range(sign[1]):
                        esign.append(j)
                        esign.append(i)
                points.extend(Sign.offset_x(esign, offset))
            offset += sign[1] + 1
            i += 1
        return points

    def collect_points(self):
        points = []
        line_len = 0
        for sign in self.signs:
            line_len += sign[1] + 1
        offset = (self.screen_len - line_len) // 2
        for sign in self.signs:
            points.extend(Sign.offset_x(sign[0], offset))
            offset += sign[1] + 1
        return points
