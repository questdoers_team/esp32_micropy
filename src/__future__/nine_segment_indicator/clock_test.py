# coding: utf-8

import neopixel
import utime

from machine import Pin

import neoindicator


def ms_to_clock(time_ms):
    time_secs = time_ms // 1000
    minutes = time_secs // 60
    seconds = time_secs % 60
    return [minutes // 10, minutes % 10, seconds // 10, seconds % 10]


display = neoindicator.NeoIndcatorClock(
    neopixel.NeoPixel(Pin(15), 39, timing=True),
)

target = utime.ticks_ms() + 36000000

while True:
    display.numbers = ms_to_clock(target - utime.ticks_ms())
    display.draw()
