# coding: utf-8

"""Module for nine segment indicator for neopixel."""

DIGITS = [
    [1, 1, 1, 1, 0, 1, 1, 1, 1],
    [0, 0, 1, 0, 0, 1, 0, 0, 1],
    [0, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 1, 1, 0, 1, 1, 0, 1, 1],
    [1, 0, 1, 1, 1, 1, 0, 0, 1],
    [1, 1, 0, 1, 1, 1, 0, 1, 1],
    [1, 1, 0, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 0, 1, 0, 0, 1],
    [1, 1, 1, 0, 1, 0, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 0, 1, 1],
]


class NeoIndicator:
    """Nine segment indicator based on neopixel."""

    def __init__(self, neopixel, number_digits):
        """
        Initialize nine segment indicator.

        :param neopixel: neopixel.NeoPixel object for indicator.
        :param number_digits: number of digits.
        """
        self.np = neopixel
        self.start = 0
        self.numbers = [0 for _ in range(number_digits)]

        self.number_digits = number_digits
        self.line_length = number_digits * 3

        self.color = (40, 0, 0)
        self.no_color = (0, 0, 0)

    def draw(self):
        """Draw actual number state."""
        for column in range(self.line_length):
            for row in range(3):
                neopixel_index = column * 3 + row
                if self.line_length <= neopixel_index < 2 * self.line_length:
                    neopixel_index = 3 * self.line_length - 1 - neopixel_index
                digit = DIGITS[self.numbers[column % self.number_digits]]
                pixel_index = (column // 3) * 3 + row
                if digit[pixel_index]:
                    self.np[self.start + neopixel_index] = self.color
                else:
                    self.np[self.start + neopixel_index] = self.no_color
        self.np.write()


class NeoIndcatorClock:
    """Nine segment indicator based on neopixel clock."""

    def __init__(self, neopixel):
        """
        Initialize nine segment indicator.

        :param neopixel: neopixel.NeoPixel object for indicator.
        """
        self.neopixel = neopixel
        self.start = 0
        self.numbers = [1, 2, 3, 4]

        self.color = (128, 0, 0)
        self.no_color = (0, 0, 0)

    def draw(self):
        """Draw actual number state."""
        for column in range(12):
            for row in range(3):
                neopixel_index = column * 3 + row
                neopixel_index += ((neopixel_index + 7) // 13)
                if 12 < neopixel_index < 26:
                    neopixel_index = 25 - (neopixel_index - 13)
                digit = DIGITS[self.numbers[column % 4]]
                pixel_index = (column // 3) * 3 + row
                if digit[pixel_index]:
                    self.neopixel[self.start + neopixel_index] = self.color
                else:
                    self.neopixel[self.start + neopixel_index] = self.no_color
        self.neopixel.write()
