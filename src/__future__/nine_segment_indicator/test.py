# coding: utf-8

import neopixel

from machine import Pin

import neoindicator

display = neoindicator.NeoIndicator(
    neopixel.NeoPixel(Pin(15), 27, timing=True),
    3,
)
display.draw()
