Buzzer
======

.. automodule:: devices.buzzer.buzzer
    :members:
    :undoc-members:
    :show-inheritance:
    :private-members:
