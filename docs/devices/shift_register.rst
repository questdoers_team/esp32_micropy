Shift register
==============

.. automodule:: devices.shift_register.shift_register
    :members:
    :undoc-members:
    :show-inheritance:
    :special-members: __call__
