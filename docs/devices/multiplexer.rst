Multiplexer
===========

.. automodule:: devices.multiplexer.multiplexer
    :members:
    :undoc-members:
    :show-inheritance:
    :special-members: __call__
