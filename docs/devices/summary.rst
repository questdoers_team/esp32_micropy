Devices
=======

This section contains the documentation for devices.

The subsections consist of description of peripheral,
its module documentation and examples of usage.

.. toctree::
   :maxdepth: 2

   buzzer.rst
   dfplayer.rst
   hcsr04.rst
   matrix_keyboard.rst
   max7219.rst
   mfrc522.rst
   multiplexer.rst
   shift_register.rst
