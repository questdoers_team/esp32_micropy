Matrix keyboard
===============

.. automodule:: devices.matrix_keyboard.matrix_keyboard
    :members:
    :undoc-members:
    :show-inheritance:
    :private-members:
