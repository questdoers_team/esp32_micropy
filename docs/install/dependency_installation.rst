Dependency installation
=======================

Installation scripts for dependencies.

MQTT
----

.. literalinclude:: ../../src/dependencies/mqtt.py
