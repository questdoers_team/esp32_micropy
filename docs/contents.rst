.. toctree::
   :maxdepth: 2

   escaperooms_architectures/summary.rst
   devices/summary.rst
   examples/summary.rst
   install/dependency_installation.rst
