Architecture version 1.
=======================

The project of a microcontroller program for an escape room has the following structure:
    * config.json
    * boot.py
    * main.py
    * app.py

These files are necessary for making a device for an escape room.

The ``config.json`` file contains the configuration of the device.

============= ===========
Key           Description
============= ===========
topic         The topic that MQTT client will subscribe
mqtt_host     The address of MQTT broker
mqtt_user     The login for MQTT broker
mqtt_password The password for MQTT broker
ssid          The SSID of the wireless network
psk           The Wi-Fi access password
============= ===========

For exapmle:

.. code:: python

    {
        "topic": "TOPIC",
        "mqtt_host": "HOST",
        "mqtt_user": "USER",
        "mqtt_password": "PASS",
        "ssid": "SSID",
        "psk": "PSK"
    }



The microcontroller executes ``boot.py`` when it starts.
It connects the device to the network using "ssid" and "psk" from config.json.
**Usually, you should not modify it.**

The microcontroller executes ``main.py`` when ``boot.py`` terminates.
It checks a network connection then it tries to connect to the MQTT broker
using credentials ("mqtt_host", "mqtt_user", "mqtt_password") from ``config.json``.

If the attempt fails the microcontroller sleeps for a minute, then restarts.

Otherwise, it subscribes on device topic, creates an ``App`` instance,
calls instance ``start()`` method for a time, and executes the infinite loop
that periodically checks income messages and calls ``loop()`` method of
App instance.
**Usually, you should not modify it.**

The ``App.py`` file contains the main class of microcontroller escape room
application.

.. class:: App(client, topic)
    :noindex:

    App is the main class of microcontroller escape room application.
    **You need to implement it's methods to make an application.**

    The arguments are:
        * ``client`` is a MQTT client.
        * ``topic`` is a MQTT topic that microcontroller is subscribed.

    The ``__init__`` should make this arguments members.

.. method:: App.on_message(topic, message)
    :noindex:

    The MQTT messages handler. The ``main.py`` calls this method
    when any MQTT message is received.

    The arguments are:
        * ``topic`` is a MQTT topic that microcontroller is subscribed
          (as bytearray). To convert to string you need to call ``.decode('utf-8')``.
        * ``message`` is a MQTT message (as string).

    Returns ``None``.

.. method:: App.start()
    :noindex:

    Make some things after connection to MQTT. Usually, this method sends greating message.
    **You should not initialize peripheral devices here. Do it in** ``__init__`` **instead.**

    Returns ``None``.

.. method:: App.loop()
    :noindex:

    Make main periodically logic.

    Returns ``None``.

File template::

    import gc
    import utime


    class App:
        def __init__(self, client, topic):
            self.client = client
            self.topic = topic

        def on_message(self, topic, message):
            print(topic)
            print(message)
            self.client.publish("micropython", "hello")

        def start(self):
            pass

        def loop(self):
            utime.sleep_ms(1000)
            print(gc.mem_free())
