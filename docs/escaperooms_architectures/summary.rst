Escaperooms Architectures
=========================

This section contains the documentation for three escaperooms architectures:

.. toctree::
   :maxdepth: 2

   version1.rst
   version2.rst
   standalone.rst

You can use the first version of the architecture for
making escape rooms with many devices that communicate over MQTT.

You can use the second version of the architecture for
making escape rooms with many devices that communicate over MQTT
with JSON packages and a central logic server.

You can use standalone architecture for making escape rooms with one ESP.

